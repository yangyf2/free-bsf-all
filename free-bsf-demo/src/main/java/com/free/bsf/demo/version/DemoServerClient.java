package com.free.bsf.demo.version;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

/**
 * RPC 版本demo
 * Robin.Wang
 * */
@RestController
@SpringBootApplication
public class DemoServerClient {
    public static void main(String[] args){
        SpringApplication.run(DemoServerClient.class, args);
    }
}
