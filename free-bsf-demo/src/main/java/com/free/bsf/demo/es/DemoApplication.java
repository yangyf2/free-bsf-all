package com.free.bsf.demo.es;

import com.google.common.collect.Lists;
import com.free.bsf.elasticsearch.base.ElasticSearchAware;
import com.free.bsf.elasticsearch.base.Param;
import com.free.bsf.elasticsearch.impl.ElasticSearchSqlProvider;
import com.free.bsf.elasticsearch.mapper.annotations.Document;
import com.free.bsf.elasticsearch.mapper.annotations.type.BooleanField;
import com.free.bsf.elasticsearch.mapper.annotations.type.DateField;
import com.free.bsf.elasticsearch.mapper.annotations.type.NumberField;
import com.free.bsf.elasticsearch.mapper.annotations.type.StringField;
import com.free.bsf.elasticsearch.mapper.enums.NumberType;
import com.free.bsf.elasticsearch.mapper.enums.StringType;
import com.free.bsf.elk.requestid.RequestUtil;
import lombok.Data;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RestController
@SpringBootApplication //implements WebMvcConfigurer
public class DemoApplication  {

	@Autowired
	private ElasticSearchSqlProvider searchService;

    @GetMapping("/selectByIndex")
    public Object selectByIndex() {
    	Integer result = searchService.searchObjectBySql("select count(*) as num from notify_index",Param.create(5), Integer.class);
    	return result;
    }

    //  SELECT * FROM indexName/type
    @GetMapping("/selectByType")
    public Object selectByType() {
		List<EsNotify> result = searchService.searchListBySql("SELECT * FROM notify_index limit 0,{}",Param.create(5),EsNotify.class);
    	return result;
    }

	@GetMapping("/selectListBySql")
	public Object selectListBySql(String sql) {
		return searchService.searchListBySql(sql,Param.create(),Object.class);
	}

	@GetMapping("/searchObjectBySql")
	public Object searchObjectBySql(String sql) {
		return searchService.searchObjectBySql(sql,Param.create(),Object.class);
	}


	@GetMapping("/insert")
    public Object insert() {
    	List<UserVo> result = Lists.newArrayList();
    	result.add(new UserVo("-1", "liu555"));
    	result.add(new UserVo("5", "liu666"));
    	result.add(new UserVo(null, "liu8888"));
    	searchService.insertData("aaa", "bbb", result);
    	return result;
    }
	@GetMapping("/deleteBySql")
	public boolean deleteBySql(String sql) {
    	searchService.deleteBySql(sql);
    	return true;
	}
    @GetMapping("/delete")
    public Object delete() {
    	try {
			searchService.deleteBySql("delete from aaa where id = {}", Param.create(5));
			return "";
		}catch (Exception ex){
    		return null;
		}
    }
    
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		MDC.put(RequestUtil.REQUEST_TRACEID, RequestUtil.getRequestId());
	}

	@Document
	@Data
	public static class EsNotify  implements ElasticSearchAware {
		@StringField(type = StringType.Keyword, store = true)
		String messageId;//消息唯一标识
		@StringField(type = StringType.Text, analyzer = "ik_smart", index = true, store = true)
		String taskName;//批量任务名
		@BooleanField(store = true)
		Boolean isTask;//是否是批量任务
		@StringField(type = StringType.Keyword, store = true)
		String templateKey;//消息模板key
		@StringField(type = StringType.Text, analyzer = "ik_smart", index = true, store = true)
		String messageParams;//消息体
		@StringField(type = StringType.Keyword, store = true)
		String receiveUser;//消息接收人
		@NumberField(type = NumberType.Integer, store = true)
		Integer retryCount;//重试次数
		@StringField(type = StringType.Keyword, store = true)
		String retryTimespan;//重试间隔
		@NumberField(type = NumberType.Integer, store = true)
		Integer failTotal;//错误记录数
		@NumberField(type = NumberType.Integer, store = true)
		Integer sendState;//发送状态
		@NumberField(type = NumberType.Integer, store = true)
		Integer receiveState;//接收状态
		@StringField(type = StringType.Keyword, store = true)
		String receiveStateDesc;//接收状态描述 add by clz.xu 2021年5月7日16:10:01
		@NumberField(type = NumberType.Integer, store = true)
		Integer effectiveTime;//有效时间
		@StringField(type = StringType.Keyword, store = true)
		String sendResult;//发送结果信息
		@DateField(store = true)
		Date createTime;//创建时间
		@DateField(store = true)
		Date updateTime;//更新时间
		@StringField(type = StringType.Keyword,store = true)
		String createUser;//创建人
		@StringField(type = StringType.Keyword,store = true)
		String updateUser;//更新人
		@NumberField(type = NumberType.Integer, store = true)
		Integer partition;//消息通道
		@StringField(type = StringType.Keyword,store = true)
		String bizId;//第三方回执Id  add by clz.xu 2021年5月7日16:10:01
		/**
		 *     实现ElasticSearchAware接口, 标识ID列
		 *     批量insert的时候会根据这个id来自动做新增或者更新
		 */
		@Override
		public String get_id() {
			return messageId;
		}
	}

}
