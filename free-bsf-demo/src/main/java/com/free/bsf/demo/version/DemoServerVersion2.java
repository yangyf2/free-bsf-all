package com.free.bsf.demo.version;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

/**
 * RPC 版本demo version 2
 * Robin.Wang
 * */
@RestController
@SpringBootApplication
public class DemoServerVersion2 {
    public static void main(String[] args){
        SpringApplication.run(DemoServerVersion2.class, args);
    }
}
