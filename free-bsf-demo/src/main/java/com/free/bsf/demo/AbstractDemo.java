package com.free.bsf.demo;

import org.springframework.web.bind.annotation.GetMapping;

public interface AbstractDemo {
    @GetMapping("/hello44444")
    Object hello444(String name);

    @GetMapping("/hello2")
    Object hello2(String name);
}
