package com.free.bsf.demo.file;
import com.free.bsf.core.http.HttpClient;
import com.free.bsf.core.thread.ThreadPool;
import com.free.bsf.core.util.FileUtils;
import com.free.bsf.core.util.HttpClientUtils;
import com.free.bsf.file.FileProvider;
import com.free.bsf.file.base.UploadToken;
import com.google.gson.JsonObject;
import lombok.val;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;

/**
 * @author Huang Zhaoping
 */
@RestController
@SpringBootApplication
public class FileApplication {

    @Autowired(required = false)
    private FileProvider fileProvider;

    @PostMapping("/upload")
    public String uploadFile(MultipartFile file) throws Exception {
        return fileProvider.upload(file.getInputStream(), file.getOriginalFilename());
    }

    @PostMapping("/uploadTemp")
    public String uploadTemp(MultipartFile file) throws Exception {
        return fileProvider.uploadTemp(file.getInputStream(), file.getOriginalFilename());
    }

    @PostMapping("/deleteFile")
    public boolean deleteFile(String url) {
        return fileProvider.delete(url);
    }

    @PostMapping("/test")
    public Boolean test(Integer max) {
        FileApplication.test2(max);
        return true;
    }

    /**
     * 获取StS 临时用户的token数据
     * @return
     */
    @PostMapping("/getStsToken")
    public UploadToken getStsToken(String name) {
        return fileProvider.uploadToken(name);
    }

    /**
     * 阿里OSS 普通上传
     * @return
     */
    @PostMapping("/uploadOss")
    public String uploadToOss(MultipartFile file) throws Exception {
        return fileProvider.upload(file.getInputStream(), file.getOriginalFilename());
    }

    @PostMapping("/uploadOss2")
    public String uploadToOss2() throws Exception {
        return fileProvider.upload(new ByteArrayInputStream(FileUtils.readAllText("C:\\tools\\a.txt").getBytes()), "a.txt");
    }

    /**
     * 阿里OSS 文件删除
     * @param url
     * @return
     */
    @PostMapping("/deleteFileOSS")
    public boolean deleteFileOSS(String url) {
        return fileProvider.delete(url);
    }

    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
       // FileApplication.test2(50);

    }

    private static void test2(int max){
        for(int i=0;i<max;i++) {
            ThreadPool.System.submit("test", () -> {
                File f = new File("C:\\Users\\Administrator\\Desktop\\新建文本文档.txt");
                try {
                    val text = FileUtils.readAllText(f.getAbsolutePath());
                    while (true) {
                        try {
                            test1(text);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }catch (Exception e1){}
            });
        }
    }

    private static void test1(String text){

        // val file = ContextUtils.getBean(FileProvider.class, false);
        try {
            val begin = System.currentTimeMillis();
            //val url = file.uploadTemp(stream, f.getName());
            try(val bs = new ByteArrayInputStream(text.getBytes())){
                val url =HttpClientUtils.system().post(//"http://localhost:8080/file/uploadTemp",
                        "https://free-api-gateway-test.free.com/free-file-provider/file/uploadTemp",
                        HttpClient.Params.custom().setContentType(ContentType.MULTIPART_FORM_DATA)
                                .addContentBody("file2",new InputStreamBody(bs,"新建文本文档.txt")).build());
                System.out.println("文件上传:"+url+" 耗时:"+(System.currentTimeMillis()-begin)+"ms");
                //System.out.println("1");
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
