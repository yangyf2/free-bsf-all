# Elk集成说明

### 介绍
ELK 在最近两年迅速崛起，成为机器数据分析，或者说实时日志处理领域，开源界的第一选择。
ELK的搭建，请参阅[ELK安装部署文档]((https://elkguide.elasticsearch.cn/) )

### 技术选型
[技术选型](doc/README-选型.md) [暂无]

### 入门篇
```
#启用elk
bsf.elk.enabled=true
```

### 进阶篇
[进阶篇](doc/README-高级.md)