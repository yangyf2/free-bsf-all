package com.free.bsf.elk;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.encoder.Encoder;

import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.elk.requestid.ElkWebTracedInterceptor;
import com.free.bsf.elk.requestid.RequestUtil;

import lombok.val;
import lombok.var;
import net.logstash.logback.appender.LogstashTcpSocketAppender;
import net.logstash.logback.encoder.LogstashEncoder;
import java.util.Arrays;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @version : 2019-05-27 14:30
 * @author: chejiangyi * 
 * @date 2020-01-15
 **/
@Configuration
@ConditionalOnProperty(name = "bsf.elk.enabled", havingValue = "true")
@Import(value = { BsfConfiguration.class })
public class ElkConfiguration {

	@ConditionalOnProperty(name = "bsf.elk.logstash.enabled", havingValue = "true")
	@Bean(initMethod = "start", destroyMethod = "stop")
	public LogstashTcpSocketAppender logstashTcpSocketAppender() {
		return new LogStashAppender().createLogStashAppender();
	}

	@ConditionalOnProperty(name = "bsf.elk.httpPrint.enabled", havingValue = "true")
	@Bean
	@ConditionalOnClass(name = "org.aspectj.lang.annotation.Aspect")
	public WebControllerAspect webControllerAspect(){
		return new WebControllerAspect();
	}

	@Bean
	@ConditionalOnProperty(name = "bsf.health.log.statistic.enabled", havingValue = "true")
	LogStatisticsFilter getLogStatisticsFilter() {
		return new LogStatisticsFilter().init();
	}

	@Bean
	@ConditionalOnProperty(name = "bsf.elk.warn.enabled", havingValue = "true")
	LogWarnFilter getLogWarnFilter() {
		return new LogWarnFilter().init();
	}


}
