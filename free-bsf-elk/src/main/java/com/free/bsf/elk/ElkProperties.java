package com.free.bsf.elk;

import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 14:05
 **/
public class ElkProperties {
    public static String[] getDestinations(){
        String address = PropertyUtils.getPropertyCache("bsf.elk.logstash.destinations","");
        if(!StringUtils.isEmpty(address)){
            return address.split(",");
        }
        return new String[]{};
    }

    public static String getAppName(){
        return PropertyUtils.getPropertyCache("bsf.elk.appName","");
    }

    public static String Prefix="bsf.elk.";
    public static String Project="Elk";
    public static String Destinations="bsf.elk.destinations";
    public static String BsfElkHttpPrintEnabled="bsf.elk.httpPrint.enabled";
    public static String BsfElkWarnEnabled="bsf.elk.warn.enabled";
    public static String BsfElkLogstashRateLimit="bsf.elk.logstash.rateLimit";
}
