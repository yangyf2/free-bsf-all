# HttpClientUtils使用文档
#### 介绍
用于快速开发Http相关功能,让开发人员无需关注性能问题,直接上手使用。  
底层实现内部已实现连接池,默认已调节至最优。

### bsf http client 相关配置
```
# Tcp是否粘包(批量封包发送)
bsf.httpclient.tcpNoDelay=true
# 总连接池大小
bsf.httpclient.maxTotal=500
# 单个host连接池大小
bsf.httpclient.defaultMaxPerRoute=500
# 连接是否需要验证有效时间
bsf.httpclient.validateAfterInactivity=10000
# 连接超时时间 【常用】
bsf.httpclient.connectTimeout=10000
# socket通讯超时时间 【常用】
bsf.httpclient.socketTimeout=15000
#请求从连接池获取超时时间
bsf.httpclient.connectionRequestTimeout=2000
#连接池共享
bsf.httpclient.connectionManagerShared=true
#回收时间间隔 s
bsf.httpclient.evictIdleConnectionsTime=60
#长连接保持时间 s
bsf.httpclient.connectionTimeToLive=-1
#是否回收
bsf.httpclient.isEvictExpiredConnections=true
#重试次数 【常用】
bsf.httpclient.retryCount=-1
```
### 快速入门篇
```
    #post json请求
    OrderRequest request = new OrderRequest();
    request.setOrderId(1L);
    HttpClient.Params params = HttpClient.Params
                .custom()
                .setContentType(ContentType.APPLICATION_JSON)
                .add(request).build();
    String resp =  HttpClientUtils.system().post(url, params);

    #get 请求
    String resp =  HttpClientUtils.system().get(url, params);
```
### 高级入门篇
```
    #仅复用连接池,自定义post或get请求
    private String request(String url){
        val post=new HttpPost(url);
        post.setHeader("Accept","application/json");
        post.setHeader("Content-Type","application/json");
        post.setEntity(new StringEntity(sql,"UTF-8"));
        val pool=HttpClientUtils.system();
        try{
            try(val rep=pool.getClient().execute(post)){
                return pool.toString(rep);
            }
        }catch(Exception e){
            throw new Exception(e);
        }
    }
```

### 压测报告
无