package com.free.bsf.core.base;

public class LimitRateException extends BsfException {
    public LimitRateException(Throwable exp)
    {
        super(exp);
    }

    public LimitRateException(String message)
    {
        super(message);
    }

    public LimitRateException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
