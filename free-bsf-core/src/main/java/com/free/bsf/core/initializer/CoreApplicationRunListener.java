package com.free.bsf.core.initializer;

import ch.qos.logback.core.util.ContextUtil;
import com.free.bsf.core.base.BsfApplicationStatus;
import com.free.bsf.core.base.Callable;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.system.ProcessExitEvent;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

public class CoreApplicationRunListener implements SpringApplicationRunListener {
    private final SpringApplication application;
    private final String[] args;

    public CoreApplicationRunListener(SpringApplication sa, String[] args) {
        this.application = sa;
        this.args = args;
    }

    @Override
    public void starting() {
        change(BsfApplicationStatus.StatusEnum.STARTING,null);
    }

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment){

    }

    @Override
    public void contextPrepared(ConfigurableApplicationContext context) {

    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {

    }

    @Override
    public void running(ConfigurableApplicationContext context) {
        change(BsfApplicationStatus.StatusEnum.RUNNING,()->{
            LogUtils.info(CoreApplicationRunListener.class, CoreProperties.Project,"应用已正常启动!");
        });
    }

    @Override
    public void failed(ConfigurableApplicationContext context, Throwable exception) {
        change(BsfApplicationStatus.StatusEnum.FAILED,()->{
            LogUtils.info(CoreApplicationRunListener.class, CoreProperties.Project,"应用启动失败!");
        });
    }

    @Override
    public void started(ConfigurableApplicationContext context) {
        change(BsfApplicationStatus.StatusEnum.STARTED,()->{
            ProcessExitEvent.register(()->{
                change(BsfApplicationStatus.StatusEnum.STOPPING,()->{
                    LogUtils.info(CoreApplicationRunListener.class, CoreProperties.Project,"应用已退出中...");
                });
            },0,false);
            ProcessExitEvent.register(()->{
                change(BsfApplicationStatus.StatusEnum.STOPPED,()->{
                    LogUtils.info(CoreApplicationRunListener.class, CoreProperties.Project,"应用已正常退出!");
                });
            },Integer.MAX_VALUE,false);
        });
    }

    public void change(BsfApplicationStatus.StatusEnum status, Callable.Action0 action0){
        if(ContextUtils.getApplicationContext()!=null&& !StringUtils.isEmpty(ContextUtils.getApplicationContext().getEnvironment().getProperty(CoreProperties.SpringApplicationName))) {
            BsfApplicationStatus.Current = status;
            for (val l : ContextUtils.getBeansByOrder(BsfApplicationStatus.StatusListener.class)) {
                l.onApplicationEvent(status);
            }
            if(action0!=null) {
                action0.invoke();
            }
        }
    }
}
