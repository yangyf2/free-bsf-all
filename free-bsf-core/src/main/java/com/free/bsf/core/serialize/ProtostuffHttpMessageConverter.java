package com.free.bsf.core.serialize;
import com.free.bsf.core.util.ProtostuffUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.WebUtils;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.Nullable;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;

/**
 * @Author 黄新宇 来自网络
 * @Date 2018/10/30 下午1:24
 * @Description feign调用中protostuff编解码器
 **/

public class ProtostuffHttpMessageConverter extends AbstractHttpMessageConverter<Object> {
    public static final MediaType PROTOBUF=new MediaType("application", "x-protobuf", Charset.forName("UTF-8"));

    public ProtostuffHttpMessageConverter() {
        super(new MediaType[]{PROTOBUF});
    }

    @Override
    protected boolean supports(Class<?> aClass) {
        return Object.class.isAssignableFrom(aClass);
    }

    @Override
    protected boolean canRead(MediaType mediaType) {
        return super.canRead(mediaType)&& checkType();
    }

    @Override
    protected boolean canWrite(MediaType mediaType) {
        return super.canWrite(mediaType)&& checkType();
    }

    //header Content-Type必须带有x-protobuf
    private boolean checkType(){
        return StringUtils.nullToEmpty(WebUtils.getRequest().getContentType()).toLowerCase().contains("x-protobuf");
    }

    @Override
    protected Object readInternal(Class<?> aClass, HttpInputMessage inputMessage) throws HttpMessageNotReadableException {
        try {
            return ProtostuffUtils.deserialize(StringUtils.toArrays(inputMessage.getBody()),aClass);
        } catch (Exception e) {
            throw new HttpMessageConversionException("protostuff解码失败" + e.getMessage(), e);
        }
    }

    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        FileCopyUtils.copy(ProtostuffUtils.serialize(object), outputMessage.getBody());
    }
}
