package com.free.bsf.core.base;

/**
 * 框架标准环境变量定义
 * @author: chejiangyi
 * @version: 2019-05-27 13:44
 **/
public enum Environment {
    /**开发环境*/
    dev("开发"),
    /**测试环境*/
    test("测试"),
    /**预发环境*/
    pre("预发"),
    /**生产环境*/
    pro("生产");

    private String name;
    Environment(String name)
    {
        this.name = name;
    }
}
