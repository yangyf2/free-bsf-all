# starter
free-bsf-starter启动项目,所有对第三方提供的类库全部都会集成到这里,并打包成一个jar包，上传到nesus中被第三方使用。  
第三方项目仅需要依赖starter jar包,便可以引用整个bsf组件,简单粗暴。

## 依赖引用
一般项目建议只依赖此jar包,快速上手bsf,简单粗暴！！！
```
<dependency>
   <artifactId>free-bsf-starter</artifactId>
   <groupId>com.free.bsf</groupId>
   <version>1.7.1-SNAPSHOT</version>
</dependency>
```

