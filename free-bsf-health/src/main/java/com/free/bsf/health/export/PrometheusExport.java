package com.free.bsf.health.export;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.health.base.HealthCheckProvider;
import com.free.bsf.health.base.Report;
import lombok.val;

public class PrometheusExport {
    public String getReportText(){
        val healthProvider = ContextUtils.getBean(HealthCheckProvider.class,false);
        if(healthProvider!=null) {
            StringBuilder sb= new StringBuilder();
            val report = healthProvider.getReport(false);
            report.eachReport((String field, Report.ReportItem reportItem) -> {
                if(reportItem!=null&&reportItem.getValue() instanceof Number)
                {
                    sb.append(field.replace(".","_")+"{desc=\""+reportItem.getDesc()+"\",} "+reportItem.getValue()+"\n");
                    //map.put(reportItem.getDesc(), reportItem.getValue().toString());
                }
                return reportItem;
            });
            return sb.toString();
        }
        return "";
    }
}
