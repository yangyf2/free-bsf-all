package com.free.bsf.health.warn;

import com.free.bsf.core.util.*;
import com.free.bsf.health.base.AbstractWarn;
import com.free.bsf.health.base.Message;
import lombok.val;

/**
 * @author: huojuncheng
 * @version: 2020-08-25 20:13
 **/
public class SystemWarn extends AbstractWarn {
    Class cls = null;
    public SystemWarn(){
        cls = ReflectionUtils.tryClassForName("com.free.bsf.message.channel.AbstractMessageProvider");
    }

    @Override
    public void notify(Message message) {
        Object baseProvider = ContextUtils.getBean(cls,false);
        if(baseProvider!=null) {
            val ip = NetworkUtils.getIpAddress();
            val text = StringUtils.subString3(message.getTitle(), 100) + "\n" +
                    "详情:" + org.springframework.util.StringUtils.trimTrailingCharacter(WebUtils.getBaseUrl(),'/')+"/bsf/health/\n"+
                    "【ip】:"+StringUtils.nullToEmpty(ip)+"\n"+
                    "【" + message.getWarnType().getDescription() + "】"+StringUtils.subString3(message.getContent(), 500);
            ReflectionUtils.callMethod(baseProvider,"sendText",new Object[]{text});
        }
    }
}
