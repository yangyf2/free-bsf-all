package com.free.bsf.health.memoryParse;

import com.free.bsf.core.util.*;
import com.free.bsf.health.config.MemoryParseProperties;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * @Classname FeignClientCustomIpAspects
 * @Description TODO
 * @Date 2021/4/28 10:03
 * @Created by chejiangyi
 */
@Aspect
public class MemoryFeignClientAspect {
    @Pointcut("@within(org.springframework.cloud.openfeign.FeignClient)")
    public void pointCut(){};

    @Around("pointCut()")
    public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
        val method = ((MethodSignature)joinPoint.getSignature()).getMethod();
        val result = joinPoint.proceed();
        long maxsize= MemoryRecordUtils.checkResultSize(result);
        if(maxsize> MemoryParseProperties.getMemoryFeignClientMaxSize()){
            //特殊场景立即报警
            WarnUtils.notify(WarnUtils.ALARM_ERROR,
                    "feignClient单次查询超过"+ MemoryParseProperties.getMemorySqlQueryMaxSize()+"条",
                    "size:"+maxsize+"\n"+
                            "web url:"+ StringUtils.nullToEmpty(WebUtils.getRequestURI())+"\n"+
                            "method path:"+method.getName());
        }
        return result;
    }


}
