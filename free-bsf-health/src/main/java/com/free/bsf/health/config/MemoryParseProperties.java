package com.free.bsf.health.config;

import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Classname MemeryProperties
 * @Description TODO
 * @Date 2021/5/12 10:50
 * @Created by chejiangyi
 */
public class MemoryParseProperties {
    /**
     *
     * @return
     */
    public static int getMemorySqlQueryMaxSize(){
        return PropertyUtils.getPropertyCache("bsf.health.memoryParse.sqlQuery.maxSize",5000);
    }
    /**
     *
     * @return
     */
    public static int getMemoryFeignClientMaxSize(){
        return PropertyUtils.getPropertyCache("bsf.health.memoryParse.feignClient.maxSize",5000);
    }


}
