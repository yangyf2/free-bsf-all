package com.free.bsf.health.export;

import com.free.bsf.core.thread.ThreadPool;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.health.base.AbstractExport;
import com.free.bsf.health.base.HealthCheckProvider;
import com.free.bsf.health.config.ExportProperties;
import com.free.bsf.health.config.HealthProperties;
import lombok.val;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: chejiangyi
 * @version: 2019-08-14 11:01
 **/
public class ExportProvider {
    private boolean isClose = true;
    protected List<AbstractExport> exports = new ArrayList<>();
    public void registerCollectTask(AbstractExport export){
        exports.add(export);
    }
    public void start() {
        isClose=false;
        if(ExportProperties.getCatEnabled()) {
            registerCollectTask(new CatExport());
        }
        if(ExportProperties.getElkEnabled()) {
            registerCollectTask(new ElkExport());
        }
        ThreadPool.System.submit("bsf系统任务:ExportProvider采集上传任务",()->{
            while (!ThreadPool.System.isShutdown()&&!isClose){
                try{
                    run();
                }catch (Exception e){
                    LogUtils.error(ExportProvider.class, HealthProperties.Project,"run 循环上传报表出错",e);
                }
                try{Thread.sleep(ExportProperties.getExportTimeSpan()*1000); }catch (Exception e){}
            }
        });
        for(val e:exports){
            try {
                e.start();
            }catch (Exception ex){
                LogUtils.error(ExportProvider.class, HealthProperties.Project,e.getClass().getName()+"启动出错",ex);
            }
        }
    }
    public void run(){
        val healthProvider = ContextUtils.getBean(HealthCheckProvider.class,false);
        if(healthProvider!=null) {
            val report = healthProvider.getReport(false);
            for (val e : exports) {
                e.run(report);
            }
        }
    }

    public void close() {
        isClose=true;
        for(val e:exports){
            try {
                e.close();
            }catch (Exception ex){
                LogUtils.error(ExportProvider.class, HealthProperties.Project,e.getClass().getName()+"关闭出错",ex);
            }
        }
    }
}
