package com.free.bsf.health.config;

import com.free.bsf.health.memoryParse.DoubtApiInterceptor;
import com.free.bsf.health.memoryParse.MemoryFeignClientAspect;
import com.free.bsf.health.memoryParse.MemorySqlMybatisInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * mybatits 监控配置文件
 * @author 	Robin.Wang
 * @date	2020-04-29
 * */
@Configuration
@ConditionalOnProperty(name = "bsf.health.memoryParse.enabled", havingValue =  "true")
public class HealthMemoryParseConfiguration {

    @Bean
    @ConditionalOnWebApplication
    public WebMvcConfigurer webMvcConfigurer() {
        return new  WebMvcConfigurer() {
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(new DoubtApiInterceptor()).addPathPatterns("/**");
            }
        };
    }
    @Bean
    @ConditionalOnProperty(name = "bsf.health.memoryParse.sql.enabled", havingValue =  "true", matchIfMissing = true)
    @ConditionalOnClass(name = "org.apache.ibatis.plugin.Interceptor")
    public MemorySqlMybatisInterceptor memorySqlMybatisInterceptor() {
        return new MemorySqlMybatisInterceptor();
    }

    @Bean
    @ConditionalOnProperty(name = "bsf.health.memoryParse.feign.enabled", havingValue =  "true", matchIfMissing = true)
    @ConditionalOnClass(name = "org.aspectj.lang.annotation.Aspect")
    public MemoryFeignClientAspect memoryFeignClientAspect(){
        return new MemoryFeignClientAspect();
    }
}
