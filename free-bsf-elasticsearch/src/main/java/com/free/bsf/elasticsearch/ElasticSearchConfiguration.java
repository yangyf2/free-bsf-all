package com.free.bsf.elasticsearch;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.pool.ElasticSearchDruidDataSourceFactory;
import com.free.bsf.core.base.BsfException;
import com.google.common.collect.Lists;
import com.free.bsf.core.base.BsfEnvironmentEnum;
import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.elasticsearch.base.ElasticSearchException;
import com.free.bsf.elasticsearch.impl.ElasticSearchProvider;
import com.free.bsf.elasticsearch.impl.ElasticSearchRestSqlProvider;
import com.free.bsf.elasticsearch.impl.ElasticSearchSqlProvider;
import lombok.val;
import lombok.var;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;

import java.net.InetAddress;
import java.util.Properties;

@Configuration
@Import(value = {BsfConfiguration.class})
@ConditionalOnProperty(name = "bsf.elasticsearch.enabled", havingValue = "true")
public class ElasticSearchConfiguration implements InitializingBean {

	
	@Override
	public void afterPropertiesSet() throws Exception {
		LogUtils.info(ElasticSearchConfiguration.class,ElasticSearchProperties.Project,"连接初始化成功!!! "+ElasticSearchProperties.BsfElasticSearchServerAddrs+"=" + ElasticSearchProperties.getServerAddrs());
	}

	@Bean(name = "bsfEsTransportClient",destroyMethod = "close")
	@Lazy
	public TransportClient getTransportClient() throws Exception{
		var serverAddrs=ElasticSearchProperties.getServerAddrs();
		if (StringUtils.isEmpty(serverAddrs)) {
			throw new ElasticSearchException("bsf.elasticsearch.serverAddrs 未配置");
		}

		String[] serverArr = serverAddrs.trim().split(",");
		if (serverArr != null && serverArr.length > 0) {
			System.setProperty("es.set.netty.runtime.available.processors", "false");
			// client初始化
			Settings settings = Settings.builder().put("client.transport.ignore_cluster_name", true).build();
			val transportClient = new PreBuiltTransportClient(settings, Lists.newArrayList());
			for (String serverAddr : serverArr) {
				String[] inetArr = serverAddr.split(":");
				transportClient.addTransportAddress(new TransportAddress(InetAddress.getByName(inetArr[0]), Integer.valueOf(inetArr[1])));
			}
			return transportClient;
		}
		return null;
	}

//	@Bean(name="bsfEsDruidDataSource",destroyMethod = "close")
//	@Lazy
	public DruidDataSource getDruidDataSource() {
		try {
			var serverAddrs = ElasticSearchProperties.getServerAddrs();
			if (org.springframework.util.StringUtils.isEmpty(serverAddrs)) {
				throw new ElasticSearchException("bsf.elasticsearch.serverAddrs 未配置");
			}

			serverAddrs = serverAddrs.trim();
			Properties properties = new Properties();
			properties.put("url", "jdbc:elasticsearch://" + serverAddrs);
			properties.put(DruidDataSourceFactory.PROP_CONNECTIONPROPERTIES, "client.transport.ignore_cluster_name=true");
			return (DruidDataSource) ElasticSearchDruidDataSourceFactory.createDataSource(properties);
		}catch (Exception e){
			throw new ElasticSearchException(e);
		}
	}
	
	@Bean(destroyMethod = "close")
	@Lazy
    public ElasticSearchProvider ElasticSearchProvider(TransportClient bsfEsTransportClient) {
        return new ElasticSearchProvider(bsfEsTransportClient);
    }

	@Bean(destroyMethod = "close")
	@Lazy
	public ElasticSearchSqlProvider ElasticSearchSqlProvider(TransportClient bsfEsTransportClient) {
		//推荐rest api 模式
		if(ElasticSearchProperties.getRestServerAddrs()!=null){
			return new ElasticSearchRestSqlProvider();
		}
		return new ElasticSearchSqlProvider(bsfEsTransportClient,getDruidDataSource());
	}

}
