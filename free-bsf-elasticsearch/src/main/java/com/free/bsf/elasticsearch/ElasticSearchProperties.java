package com.free.bsf.elasticsearch;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: chejiangyi
 * @version: 2019-08-12 12:17
 **/
public class ElasticSearchProperties {
    // es服务地址,多个用逗号分隔
    public static String getServerAddrs(){
        return PropertyUtils.getPropertyCache("bsf.elasticsearch.serverAddrs","");
    }

    public static String getRestServerAddrs(){
        return PropertyUtils.getPropertyCache("bsf.elasticsearch.restServerAddrs","");
    }

    public static Integer getBulkSize(){
        return PropertyUtils.getPropertyCache("bsf.elasticsearch.bulkSize",100);
    }

    public static String Prefix="bsf.elasticsearch.";
    public static String Project="ElasticSearch";
    public static String BsfElasticSearchServerAddrs="bsf.elasticsearch.serverAddrs";
    public static String BsfElasticSearchBulkSize="bsf.elasticsearch.bulkSize";
    public static String BsfElasticSearchPrintSql="bsf.elasticsearch.printSql";
    public static String SpringApplicationName="spring.application.name";
    public static String ManagementHealthElasticSearchEnabled = "management.health.elasticsearch.enabled";
}
