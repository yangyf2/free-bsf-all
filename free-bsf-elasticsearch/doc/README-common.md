# ElasticSearch 原生模式支持

### 介绍
ElasticSearch 采用原生模式进行连接,支持es原生的一些es操作封装,方便熟悉原生api操作的人进行开发。

## 配置说明
连接ElasticSearch配置
```
### 配置ES服务地址多个逗号隔开（使用原生的方式连接ES）
bsf.elasticsearch.serverAddrs = 127.0.0.1:9300
###  ES批提交最小数量, 默认100
bsf.elasticsearch.bulkSize = 100
```
## 代码示例
* 1.定义索引对应的实体对象,并基于该对象定义索引的Mapping规则
   mapping build部分参考了https://github.com/gitchennan/elasticsearch-mapper的设计思路,使用声明式注解定义mapping规则
   在原作者基础上升级了ElasticSearch的版本到6.7.1,并且移除了一些新版本废弃的特性
   对mapper部分进行了抽象重写

```

@Document
public class OrderVo implements ElasticSearchAware {
    
    @NumberField(type = NumberType.Integer)
    private Integer orderId;
    
    @StringField(type = StringType.Keyword)
    private String orderNo;
    
    @StringField(type = StringType.Text, analyzer = "ik_smart", index = true, store = false)
    private String productName;
    
    /**
     * 下单人 没注解也会自动映射
     */
    private String createBy;
    
    @NumberField(type = NumberType.Double)
    private BigDecimal amount;
    
    /**
     *  下单地址
     */
    @GeoPointField
    private GeoPoint orderPostion;
    
    /*
     * 单字段多映射
     */
    @MultiField(
            mainField = @StringField(type = StringType.Keyword, boost = 2.0f),
            fields = {
                    @MultiNestedField(name = "cn", field = @StringField(type = StringType.Text, analyzer = "ik_smart")),
                    @MultiNestedField(name = "en", field = @StringField(type = StringType.Text, analyzer = "english")),
            },
            tokenFields = {
                    @TokenCountField(name = "cnTokenCount", analyzer = "ik_smart")
            }
    )
    private String orderDesc;
    
    /**
     *     实现ElasticSearchAware接口, 标识ID列  
     *     批量insert的时候会根据这个id来自动做新增或者更新
     */
    @Override
    public String get_id() {
        return orderId.toString();
    }
    /... getter and setter .../
 }
```

* 2.创建ElasticSearch索引,设置mapping

```    
    @Autowired
    private ElasticSearchProvider searchProvider;
    
    
    /*
     * 方式一-----创建index的时候设置mapping
     */
    public void createIndex(){
        searchProvider.createIndex("orderindex", "ordertype", OrderVo.class);
    }
    
    /*
     * 方式二第一步-----单独创建索引,不设置mapping
     */
    public void createIndexWithoutMapping(){
        searchProvider.createIndex("orderindex", "ordertype");
    }
    
    /*
     * 方式二第二步-----为已有索引设置mapping
     */
    public void putMapping(){
        searchProvider.putMapping("orderindex", "ordertype", OrderVo.class);
    }
  ```
