package com.free.bsf.swagger.config;

import com.free.bsf.core.config.BsfConfiguration;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.swagger.strategy.SwaggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@Import(BsfConfiguration.class)
@EnableConfigurationProperties(SwaggerProperties.class)
@ConditionalOnProperty(name = "bsf.swagger.enabled", havingValue = "true")
@EnableSwagger2
public class SwaggerConfiguration implements InitializingBean {
    @Bean
    public Docket customDocket() {
        return SwaggerFactory.chooseStrategy().get();
    }

    @Override
    public void afterPropertiesSet() {
        LogUtils.info(SwaggerConfiguration.class,SwaggerProperties.Project,"已启动!!!");
    }

}
