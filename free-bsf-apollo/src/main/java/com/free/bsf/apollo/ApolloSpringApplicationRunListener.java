package com.free.bsf.apollo;

import lombok.val;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import java.util.ArrayList;

/**
 * @Classname Apollo
 * @Description apollo 启动监听器
 * @Date 2021/3/31 17:32
 * @Created by chejiangyi
 */
public class ApolloSpringApplicationRunListener implements SpringApplicationRunListener {
    public ApolloSpringApplicationRunListener(SpringApplication application, String[] args) {
        val list = new ArrayList<ApplicationContextInitializer<?>>();
        application.getInitializers().forEach(c -> {
            if (c instanceof com.ctrip.framework.apollo.spring.boot.ApolloApplicationContextInitializer&&
                    !(c instanceof ApolloApplicationContextInitializerExtend)) {
                list.add(new ApolloApplicationContextInitializerExtend());
            } else {
                list.add(c);
            }
        });
        application.setInitializers(list);
    }
    public void starting(){
    }

    public void environmentPrepared(ConfigurableEnvironment environment){
    }

    public void contextPrepared(ConfigurableApplicationContext context){}

    public void contextLoaded(ConfigurableApplicationContext context){}

    public void started(ConfigurableApplicationContext context){}

    public void running(ConfigurableApplicationContext context){}

    public void failed(ConfigurableApplicationContext context, Throwable exception){}
}
