package com.free.bsf.apollo;

import org.springframework.context.ConfigurableApplicationContext;

/**
 * @Classname ApolloApplicationContextInitializerExtend
 * @Description 重写ApolloApplicationContextInitializer扩展
 * @Date 2021/3/31 17:49
 * @Created by chejiangyi
 */
public class ApolloApplicationContextInitializerExtend extends com.ctrip.framework.apollo.spring.boot.ApolloApplicationContextInitializer {
    public ApolloApplicationContextInitializerExtend(){
        super();
    }
    @Override
    public void initialize(ConfigurableApplicationContext context){
        super.initialize(context);
        ApolloExtend.refreshApolloPropertySources();
    }
}
