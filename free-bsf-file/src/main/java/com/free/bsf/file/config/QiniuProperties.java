package com.free.bsf.file.config;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;

/**
 * @author Huang Zhaoping
 */
public class QiniuProperties {

    public static String getAccessKey(){
        return PropertyUtils.getPropertyCache("bsf.file.qiniu.accessKey","");
    }
    public static String getSecurityKey(){
        return PropertyUtils.getPropertyCache("bsf.file.qiniu.securityKey","");
    }
    public static String getBucketName(){
        return PropertyUtils.getPropertyCache("bsf.file.qiniu.bucketName","");
    }
    public static String getBucketUrl(){
        return PropertyUtils.getPropertyCache("bsf.file.qiniu.bucketUrl","");
    }
    public static String getTempDir(){
        return PropertyUtils.getPropertyCache("bsf.file.qiniu.tempDir","");
    }

}
