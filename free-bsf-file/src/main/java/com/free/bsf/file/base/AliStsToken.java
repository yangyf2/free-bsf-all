package com.free.bsf.file.base;

import lombok.Builder;
import lombok.Data;

/**
 * @description: STS验证返回对象
 * @Author YH
 * @Date 2021/12/6 18:06
 */
@Data
//@Builder
public class AliStsToken {
     String accessKeyId;
     String accessKeySecret;
     String securityToken;
     String bucketName;
}
