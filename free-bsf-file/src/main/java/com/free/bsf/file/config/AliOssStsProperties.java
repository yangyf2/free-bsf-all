package com.free.bsf.file.config;

import com.free.bsf.core.util.PropertyUtils;
import lombok.Data;

/**
 * @description: 阿里oss相关参数配置
 * @Author YH
 * @Date 2021/12/3 17:51
 */
public class AliOssStsProperties {
    public static String getEndpoint(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.endpoint","");
    }
    public static String getRegion(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.region","");
    }
    public static String getBucketName(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.bucketName","");
    }
    public static String getAccessKeyId(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.accessKeyId","");
    }
    public static String getAccessKeySecret(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.accessKeySecret","");
    }
    //RAM角色
    public static String getRoleArn(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.roleArn","");
    }
    //自定义临时名称
    public static String getRoleSessionName(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.roleSessionName","");
    }
    //自定义的bucketName下的目录
    public static String getTempDir(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.tempDir","");
    }
    public static String getBucketUrl(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.bucketUrl","");
    }
    //单位:秒
    public static Long getTimeOut(){
        return PropertyUtils.getPropertyCache("bsf.file.aliossSts.timeOut",15*60L);
    }
}
