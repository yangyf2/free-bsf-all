## 阿里云集成进阶篇
阿里云文件服务采用[sts模式认证](https://help.aliyun.com/document_detail/100624.htm )模式实现文件前后端上传和删除。
### 参考文档
* [错误: Specified access key is not found](https://help.aliyun.com/document_detail/43000.html)
* [Error message: You are not authorized to do this action. You should be authorized by RAM.](https://www.alibabacloud.com/help/zh/resource-access-management/latest/faq-about-ram-roles-and-sts-tokens)

### 配置说明
```
## bsf file  集成
##file服务的启用开关，非必须，默认true
bsf.file.enabled=true 			
##file服务默认为七牛云，可以扩展其他的第三方文件服务或者自己实现
bsf.file.provider=aliossSts 	
##文件上传的重试次数，用于异常情况下的重试，如网络异常
bsf.file.retryUpload=3
bsf.file.aliossSts.region = cn-hangzhou
bsf.file.aliossSts.accessKeyId = LTAI5tHdRB7gyJi******
bsf.file.aliossSts.accessKeySecret = owrFCZIaSBBbPKlc******
#获取的角色ARN
bsf.file.aliossSts.roleArn = acs:ram::1069735822596087:role/oss-file
#自定义角色会话名称，用来区分不同的令牌，例如可填写为SessionTest。 
bsf.file.aliossSts.roleSessionName = free-sts
#阿里云oss桶信息
bsf.file.aliossSts.bucketName = linkhub-dev
#阿里云oss sts失效时间
bsf.file.aliossSts.timeOut = 60
#STS接入地址，例如sts.cn-hangzhou.aliyuncs.com
bsf.file.aliossSts.endpoint = oss-cn-hangzhou.aliyuncs.com
#阿里云oss外网访问的域名地址
bsf.file.aliossSts.bucketUrl = https://linkhub-dev.oss-cn-hangzhou.aliyuncs.com/ 

```

### 使用示例
+ 快速上手版
```
	@Autowired(required = false)
	private FileProvider fileProvider;

	/**
	*  文件上传
	*/
    @PostMapping("/upload")
    public CommonResponse<String> uploadFile(MultipartFile file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file.getInputStream(), file.getOriginalFilename()));
    }
	/**
	* 上传服务器本地文件
	*/    
    public CommonResponse<String> uploadLocalFile(String file) throws Exception {    	
    	return CommonResponse.success(fileProvider.upload(file, FileUtils.getFileName(file)));
    }
```
+ 核心服务api
```
#文件服务支持的接口能力
public interface FileProvider {

    /**
     * 上传永久文件
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String upload(InputStream input, String name);

    /**
     * 	上传永久文件
     *
     * @param filePath 上传的本地文件名
     */
    String upload(String filePath);
    /**
     * 上传临时文件(保留n个月,最终会过期自动删除)
     *
     * @param input 文件流
     * @param name  文件名
     * @return
     */
    String uploadTemp(InputStream input, String name);
    /**
     * 	上传临时文件
     *
     * @param filePath 上传的本地文件名
     */
    String uploadTemp(String filePath);

    /**
     * 上传token,用于前端上传的临时token
     * @return
     */
    UploadToken uploadToken(String name);

    /**
     * 删除文件
     *
     * @param url
     * @return
     */
    boolean delete(String url);
}
```
### UploadToken 格式参考
token包含accessKeyId(临时分配),accessKeySecret(临时分配),securityToken等信息为json格式。
Example:
```
{
	"token": "{\"accessKeyId\":\"STS.NT6TZBB85pKUhcoAgpPKRNUMV\",\"accessKeySecret\":\"A7KPqTNataJQu5jJoNfhQhdeR5x3yKgMddE6D5ivQYRW\",\"securityToken\":\"CAIS7wF1q6Ft5B2yfSjIr5eDH+D2r+cUx4m+akXepWclXMR+oZDmtDz2IHFPenJvB+0YsP80mWtX6fsTlqF9UZJIAFfYdpPUUV7/AlvzDbDasumZsJYm6vT8a0XxZjf/2MjNGZabKPrWZvaqbX3diyZ32sGUXD6+XlujQ/br4NwdGbZxZASjaidcD9p7PxZrrNRgVUHcLvGwKBXn8AGyZQhKwlMk1zIgsvnvmJfBu0SG3ALAp7VL99irEP+NdNJxOZpzadCx0dFte7DJuCwqsEkRqvwp0vEap2yc44rCXAQA+XWBKPGR/NxrIR/moRbvx1vfxRqAAUyJllxoDuFF9wO6kFDE9r9B699bpZnOy5yecVWBlYY4xV1vC/YmN3GPKND0+l1xNsa9BDxbXC0sbJeMjtJhxXejXqucEkke87UErKkDkJBD3ykCP8sP+XVOSTO6NIGcWFDQq4aDf3QDKuwxUEdirEpgvnmCqVYVcnkbKMaqzfZ2\"}",
	"key": "docs/20220701/7D8F4060EF7A4E13B1D0C5083D811F61.txt",
	"domain": "https://linkhub-dev.oss-cn-hangzhou.aliyuncs.com/"
}
```
