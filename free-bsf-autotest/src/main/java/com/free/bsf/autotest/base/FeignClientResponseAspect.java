package com.free.bsf.autotest.base;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.core.util.WebUtils;
import feign.Response;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @Classname FeignClientCustomIpAspects
 * @Description TODO
 * @Date 2021/4/28 10:03
 * @Created by chejiangyi
 */
@Aspect
public class FeignClientResponseAspect {
    @Pointcut("execution(* org.springframework.cloud.openfeign.support.SpringDecoder.decode(..))")
    public void pointCut(){};

    @Around("pointCut()")
    public Object handle(ProceedingJoinPoint joinPoint) throws Throwable {
        if(WebUtils.getRequest()!=null&&joinPoint.getArgs()[0]!=null && joinPoint.getArgs()[0] instanceof Response)
        {
            val webRequest =  WebUtils.getRequest();
            val response = (Response)joinPoint.getArgs()[0];
            val operatorType = response.headers().get(AutoTestProperties.AutoTestOperatorType);
            if(operatorType.size()>0) {
                if (operatorType.contains(OperatorTypeEnum.operator.getName())){
                    AutoTestUtil.setOperatorType(webRequest, OperatorTypeEnum.operator.getName());
                }else if(operatorType.contains(OperatorTypeEnum.queryOnly.getName())){
                    AutoTestUtil.setOperatorType(webRequest, OperatorTypeEnum.queryOnly.getName());
                }else{
                    AutoTestUtil.setOperatorType(webRequest, OperatorTypeEnum.none.getName());
                }
            }
        }
        return joinPoint.proceed();
    }


}
