package com.free.bsf.autotest.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestInfo {
    String url;
    String header;
    String body;
    String fromIp;
    String appName;
    String traceId;
    String traceTop;
    Date createTime;
    String method;
    String operatorType;
    String attribute;
}
