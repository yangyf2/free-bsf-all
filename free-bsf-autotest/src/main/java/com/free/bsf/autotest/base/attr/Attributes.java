package com.free.bsf.autotest.base.attr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class Attributes {
    @AllArgsConstructor
    public static enum LevelEnum {
        CORE("核心"),
        MAIN("主要"),
        COMMON("普通"),
        MINOR("次要"),
        REPORT("报表");
        @Getter
        @Setter
        private String name;
    }

    @AllArgsConstructor
    public static enum TestEnum {
        NONE("无"),
        NEED("需要"),
        SKIP("跳过");
        @Getter
        @Setter
        private String name;
    }


    @AllArgsConstructor
    public static enum ApiTypeEnum {
        NONE("无"),
        QUERY("查询"),
        OPERATOR("操作");
        @Getter
        @Setter
        private String name;
    }
}
