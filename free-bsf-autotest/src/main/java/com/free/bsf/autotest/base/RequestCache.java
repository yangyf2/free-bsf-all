package com.free.bsf.autotest.base;

import com.free.bsf.autotest.AutoTestProperties;
import com.free.bsf.core.util.PropertyUtils;
import lombok.val;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestCache {
    private ConcurrentLinkedQueue<RequestInfo> cached = new ConcurrentLinkedQueue<>();
    private Object pollLock = new Object();
    private AtomicInteger size = new AtomicInteger(0);
    public void add(RequestInfo requestInfo){
        //超出则丢弃
        if(size.get()> PropertyUtils.getPropertyCache(AutoTestProperties.AutoTestRecordCacheMax,5000))
            return;
        cached.add(requestInfo);
        size.incrementAndGet();
    }
    public RequestInfo[] poll(){
        synchronized (pollLock) {
            val r = cached.toArray(new RequestInfo[0]);
            cached.clear();//允许丢失部分数据
            size.set(0);
            return r;
        }
    }
}
