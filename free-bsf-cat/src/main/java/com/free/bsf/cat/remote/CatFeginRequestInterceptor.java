package com.free.bsf.cat.remote;

import com.dianping.cat.Cat;
import com.free.bsf.core.apiregistry.CoreRequestInterceptor;
import com.free.bsf.core.apiregistry.RequestInfo;
import com.free.bsf.core.util.WebUtils;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.val;

import java.util.HashMap;
import java.util.Map;

/**
 * 调用链拦截器
 * @author: chejiangyi
 * @version: 2019-08-30 13:21
 **/
public class CatFeginRequestInterceptor implements RequestInterceptor, CoreRequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                requestTemplate.header(kv.getKey(),kv.getValue());
            }
        }
    }

    private Map<String,String> newHeader(){
        Map<String,String> header = new HashMap<>();
        CatRemoteContext context = new CatRemoteContext();
        Cat.logRemoteCallClient(context);
        header.put(Cat.Context.ROOT, context.getProperty(Cat.Context.ROOT));
        header.put(Cat.Context.CHILD, context.getProperty(Cat.Context.CHILD));
        header.put(Cat.Context.PARENT,context.getProperty(Cat.Context.PARENT));
        header.put("application.name", Cat.getManager().getDomain());
        return header;
    }

    @Override
    public void append(RequestInfo request) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                request.getHeader().put(kv.getKey(),kv.getValue());
            }
        }
    }

}
