package com.free.bsf.api.apiregistry;

import java.lang.annotation.*;

/**
 * api忽略特定方法
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface ApiIgnore {
}

