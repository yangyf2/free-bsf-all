package com.free.bsf.message.channel;

import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.message.MessageProperties;
import lombok.val;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Classname BaseProvider
 * @Date 2021/4/23 17:46
 * @Created by chejiangyi
 */
public abstract class AbstractMessageProvider {
    protected String getName(){
        return "";
    }
    public void sendText(String text){}

    protected boolean isEnabled(){
        return PropertyUtils.getPropertyCache(MessageProperties.MessageEnabled,false);
    }

    protected List<String> getTokens(){
        val token = PropertyUtils.getPropertyCache(MessageProperties.MessageToken.replace("{type}",getName()),"");
        val projectToken = PropertyUtils.getPropertyCache(MessageProperties.MessageProjectToken.replace("{type}",getName()),"");
        val tokens = new ArrayList<String>();
        tokens.addAll(Arrays.asList(token.split(",")));
        tokens.addAll(Arrays.asList(projectToken.split(",")));
        return tokens.stream().filter(c-> !StringUtils.isEmpty(c)).collect(Collectors.toList());
    }
}
