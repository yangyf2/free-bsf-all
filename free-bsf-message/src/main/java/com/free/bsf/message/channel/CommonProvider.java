package com.free.bsf.message.channel;

import com.free.bsf.core.http.DefaultHttpClient;
import com.free.bsf.core.http.HttpClient;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.message.MessageProperties;
import org.apache.http.entity.ContentType;

import java.net.SocketTimeoutException;

/**
 * @Classname CommonProvider
 * @Date 2021/4/23 18:15
 * @Created by chejiangyi
 */
public class CommonProvider extends AbstractMessageProvider {
    @Override
    protected String getName(){
        return "common";
    }

    @Override
    public void sendText(String text){
        if(!isEnabled())
            return;
        getTokens().forEach(t-> {
            String api = PropertyUtils.getPropertyCache(MessageProperties.MessageCommonUrl,"").replace("{access_token}", t);
            String content=PropertyUtils.getPropertyCache(MessageProperties.MessageCommonContent,"").replace("{text}", text);
            try {
                HttpClient.Params params = HttpClient.Params.custom()
                        .setContentType(ContentType.APPLICATION_JSON)
                        .add(content)
                        .build();
                DefaultHttpClient.Default.post(api, params);
            } catch (Exception e) {
                if (e.getCause() instanceof SocketTimeoutException) {
                    //网关不一定稳定
                    return;
                }
                throw e;
            }
        });
    }
}
