package com.free.bsf.message;

import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.PropertyUtils;
import com.free.bsf.message.channel.*;
import lombok.val;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author: chejiangyi
 * @version: 2019-05-31 13:18
 **/
@Configuration
@ConditionalOnProperty(name = "bsf.message.enabled", havingValue = "true")
public class MessageConfiguration implements InitializingBean {

    @Bean
    public AbstractMessageProvider messageProvider() {
       val type = PropertyUtils.getPropertyCache(MessageProperties.MessageType,"qiyeweixin");
       if("dingding".equalsIgnoreCase(type)){
           return new DingdingProvider();
       }else if("flybook".equalsIgnoreCase(type)){
           return new FlyBookProvider();
       }else if("common".equalsIgnoreCase(type)){
            return new CommonProvider();
       }
       else{
           return new QiYeWeiXinProvider();
       }
    }

    @Override
    public void afterPropertiesSet(){
        LogUtils.info(MessageConfiguration.class,MessageProperties.Project,"已启动!!!");
    }
}
