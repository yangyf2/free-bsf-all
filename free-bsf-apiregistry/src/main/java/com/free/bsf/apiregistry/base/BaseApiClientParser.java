package com.free.bsf.apiregistry.base;

import com.free.bsf.core.apiregistry.RequestInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.aspectj.lang.ProceedingJoinPoint;

import java.lang.reflect.Method;

/**
 * @ApiClient解析器
 */
public class BaseApiClientParser {
    @AllArgsConstructor
    @Data
    public static class ApiClientParserInfo{
        String url;
        Method method;
        ProceedingJoinPoint joinPoint;
    }
    public RequestInfo parse(ApiClientParserInfo info){
        return null;
    }
}
