package com.free.bsf.apiregistry.base;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.core.base.BsfApplicationStatus;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.initializer.CoreApplicationRunListener;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.LogUtils;
import lombok.val;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ApiRegistryHealthFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        val request = (HttpServletRequest) servletRequest;
        val response = (HttpServletResponse) servletResponse;
        val contextPath = org.springframework.util.StringUtils.trimTrailingCharacter(request.getContextPath(), '/');
        val uri = request.getRequestURI();
        /*下线apiRegistry,一般在k8s CICD中使用*/
        if(uri.startsWith(contextPath+"/bsf/eureka/offline/")||uri.startsWith(contextPath+"/bsf/apiRegistry/offline/")) {
            val registry = ContextUtils.getBean(BaseRegistry.class,false);
            if(registry!=null){
                registry.close();
                write(response,"已下线");
                val listener = ContextUtils.getBean(CoreApplicationRunListener.class,false);
                if(listener!=null) {
                    listener.change(BsfApplicationStatus.StatusEnum.STOPPING, () -> {
                        LogUtils.info(ApiRegistryHealthFilter.class, CoreProperties.Project, "apiRegistry 设置当前应用程序为退出中...");
                    });
                }
                LogUtils.info(ApiRegistryHealthFilter.class, ApiRegistryProperties.Project,"apiRegistry 服务被强制下线!");
            }
        }
        /*apiRegistry服务注册列表*/
        else if(uri.startsWith(contextPath+"/bsf/apiRegistry/")){
            val registry = ContextUtils.getBean(BaseRegistry.class,false);
            if(registry!=null) {
                val report = registry.getReport();
                write(response,report.replaceAll("\r","").replace("\n","<br/>"));
            }
        }
        else {
            chain.doFilter(servletRequest, servletResponse);
        }

    }

    private void write(HttpServletResponse response,String text) throws IOException {
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().append(text);
        response.getWriter().flush();
        response.getWriter().close();
    }
    @Override
    public void destroy() {

    }
}
