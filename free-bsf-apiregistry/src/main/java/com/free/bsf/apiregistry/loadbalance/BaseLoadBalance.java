package com.free.bsf.apiregistry.loadbalance;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiClientInfo;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.ThreadUtils;
import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 负载均衡器定义
 */
public class BaseLoadBalance {

    public BaseLoadBalance(){
        ThreadUtils.system().submit("apiRegistry 故障恢复定时任务",()->{
            while (!ThreadUtils.system().isShutdown()) {
                if(failsList.size()>0) {
                    synchronized (failsListLock) {
                        failsList.clear();
                    }
                }
                ThreadUtils.sleep(ApiRegistryProperties.getRegistryFailRetryTimeSpan());
            }
        });
    }
    /**可用负载均衡节点*/
    public String getAvailableHostPort(String appName){
        return null;
    }
    /**添加失败节点*/
    public void addFail(String appName,String hostPort){
        synchronized (failsListLock) {
            if (!failsList.containsKey(appName)) {
                failsList.put(appName, new ArrayList<>());
            }
            val list = failsList.get(appName);
            if(!list.contains(hostPort))
                list.add(hostPort);
        }
    }

    protected ConcurrentHashMap<String,List<String>> failsList =new ConcurrentHashMap<>();
    protected Object failsListLock = new Object();

    protected List<String> getAvailableHostPortList(String appName){
        List rs = new ArrayList<String>();
        //配置优先
        for(val s : ApiRegistryProperties.getAppNameHosts(appName)){
            if(!StringUtils.isEmpty(s))
            {rs.add(s);}
        }
        if(rs.size()>0){
            return rs;
        }
        //注册中心
        val registry = ContextUtils.getBean(BaseRegistry.class,false);
        if(registry!=null) {
            val list = registry.getServerList().get(appName);
            if(list!=null) {
                for (val s : list) {
                    if (!StringUtils.isEmpty(s)) {
                        rs.add(s);
                    }
                }
            }
        }
        //移除错误
        val fails = failsList.get(appName);
        if(fails!=null){
            rs.removeAll(fails);
        }
        return rs;
    }
}
