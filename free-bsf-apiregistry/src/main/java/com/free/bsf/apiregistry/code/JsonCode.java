package com.free.bsf.apiregistry.code;

import com.fasterxml.jackson.core.type.TypeReference;
import com.free.bsf.core.util.JsonUtils;
import com.free.bsf.core.util.StringUtils;
import lombok.val;

import java.lang.reflect.Type;
import java.nio.charset.Charset;

public class JsonCode implements ICode {
    private Charset charset = Charset.forName("utf-8");
    public <T> byte[] encode(T data){
        return JsonUtils.serialize(data).getBytes(charset);
    }
    public <T> T decode(byte[] data,Type type){
        return JsonUtils.deserialize(new String(data,charset),type);
    }
}
