package com.free.bsf.apiregistry.rpcclient;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;

public class RpcClientFactory {
    public static IRpcClient create(){
        if(HttpUrlConnectionRpcClient.class.getSimpleName().equalsIgnoreCase(ApiRegistryProperties.getRpcClientType())){
            return new HttpUrlConnectionRpcClient();
        }
        if(HttpClientRpcClient.class.getSimpleName().equalsIgnoreCase(ApiRegistryProperties.getRpcClientType())){
            return new HttpClientRpcClient();
        }
        throw new ApiRegistryException("请配置bsf.apiRegistry.httpClient.type");
    }
}
