package com.free.bsf.apiregistry.loadbalance;

import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiRegistryException;
import com.free.bsf.apiregistry.registry.BaseRegistry;
import com.free.bsf.apiregistry.registry.RedisRegistry;
import com.free.bsf.core.util.ContextUtils;
import com.free.bsf.core.util.StringUtils;
import com.free.bsf.core.util.ThreadUtils;
import lombok.val;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class LoadBalanceFactory {
    public static BaseLoadBalance create(){
        if(RoundRobinLoadBalance.class.getSimpleName().equalsIgnoreCase(ApiRegistryProperties.getRegistryLoadBalanceType())){
            return new RoundRobinLoadBalance();
        }
        throw new ApiRegistryException("请配置bsf.apiRegistry.loadBalance.type");
    }
}
