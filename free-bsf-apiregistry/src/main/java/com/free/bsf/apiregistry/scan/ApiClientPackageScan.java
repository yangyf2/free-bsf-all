package com.free.bsf.apiregistry.scan;

import com.free.bsf.api.apiregistry.ApiClient;
import com.free.bsf.apiregistry.ApiRegistryProperties;
import com.free.bsf.apiregistry.base.ApiUtils;
import com.free.bsf.core.base.BsfException;
import com.free.bsf.core.util.LogUtils;
import com.free.bsf.core.util.ReflectionUtils;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.annotation.FullyQualifiedAnnotationBeanNameGenerator;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @ApiClient 包扫描初始化注入bean
 */
@SuppressWarnings("unchecked")
public class ApiClientPackageScan  {
    public static class ApiClientAnnotationPackageScan implements ImportBeanDefinitionRegistrar {
        @Override
        public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
            String[] basePackages = ApiRegistryProperties.getRpcClientBasePackages();
            if(basePackages.length>0) {
                //自定义的 包扫描器
                ApiClientPackageScanHandle scanHandle = new ApiClientPackageScanHandle(beanDefinitionRegistry, false);
                //扫描指定路径下的接口
                scanHandle.doScan(basePackages);
            }
        }
    }

    public static class ApiClientPackageScanHandle extends ClassPathBeanDefinitionScanner {
        public ApiClientPackageScanHandle(BeanDefinitionRegistry registry, boolean useDefaultFilters) {
            super(registry, useDefaultFilters);
            if(ApiRegistryProperties.getRpcClientTypeBeanNameEnabled()) {
                this.setBeanNameGenerator(new FullyQualifiedAnnotationBeanNameGenerator());
            }
        }

        @Override
        protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
            //添加过滤条件
            addIncludeFilter(new AnnotationTypeFilter(ApiClient.class));
            //兼容@FeignClient
            Class feignClientCls= ApiUtils.FeignClientAnnotationClass;
            if(feignClientCls!=null) {
                addIncludeFilter(new AnnotationTypeFilter(feignClientCls));
            }
            Set<BeanDefinitionHolder> beanDefinitionHolders = super.doScan(basePackages);
            if (beanDefinitionHolders.size() != 0) {
                //给扫描出来的接口添加上代理对象
                processBeanDefinitions(beanDefinitionHolders);
            }
            return beanDefinitionHolders;
        }

        /**
         * 给扫描出来的接口添加上代理对象
         * @param beanDefinitions
         */
        private void processBeanDefinitions(Set<BeanDefinitionHolder> beanDefinitions) {
            GenericBeanDefinition definition;
            for (BeanDefinitionHolder holder : beanDefinitions) {
                definition = (GenericBeanDefinition) holder.getBeanDefinition();
                //拿到接口的全路径名称
                String beanClassName = definition.getBeanClassName();

                //设置属性 即所对应的消费接口
                try {
                    definition.getPropertyValues().add("interfaceClass", Class.forName(beanClassName));
                    //设置Calss 即代理工厂
                    definition.setBeanClass(MethodProxyFactory.class);
                    //按 照查找Bean的Class的类型
                    definition.setAutowireMode(GenericBeanDefinition.AUTOWIRE_BY_TYPE);

                } catch (ClassNotFoundException e) {
                    throw new BsfException(e);
                }
            }
        }

        @Override
        protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
            return beanDefinition.getMetadata().isInterface() && beanDefinition.getMetadata().isIndependent();
        }
    }

    public static class MethodProxyFactory<T> implements FactoryBean<T> {
        @Getter @Setter
         Class<T> interfaceClass;//所对应的消费接口
        @Override
        public T getObject() {
            return (T) newInstance(interfaceClass);//通过对应的消费接口返回代理类
        }

        @Override
        public Class<?> getObjectType() {
            return interfaceClass;
        }

        @Override
        public boolean isSingleton() {
            return true;
        }

        @SuppressWarnings("unchecked")
        private static <T> T newInstance(Class<T> methodInterface) {
            final MethodProxy<T> methodProxy = new MethodProxy<>(methodInterface);
            return (T) Proxy.newProxyInstance(
                    Thread.currentThread().getContextClassLoader(),
                    new Class[]{methodInterface},
                    methodProxy);
        }

    }

    public static class MethodProxy<T> implements InvocationHandler {
        Class<T> interfaceClass;
        public MethodProxy(Class<T> interfaceClass) {
            this.interfaceClass=interfaceClass;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            try {
                //支持接口default默认实现
                if(ReflectionUtils.isDefaultMethod(method)){
                    return ReflectionUtils.invokeDefaultMethod(proxy,method,args);
                }
                if(method.getName().equals("toString")){
                    return this.toString();
                }
                LogUtils.error(MethodProxy.class, ApiRegistryProperties.Project,"未找到方法实现:" + getFullName(method));
                return null;
            }catch (Exception e){
                LogUtils.error(MethodProxy.class,ApiRegistryProperties.Project,"方法调用出错:" + getFullName(method));
                return null;
            }
        }

        private String getFullName(Method method){
            return method.getDeclaringClass().getName()+"."+method.getName();
        }
    }
}
