package com.free.bsf.apiregistry.rpcclient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.free.bsf.core.apiregistry.RequestInfo;

import java.lang.reflect.Type;

/**
 * IRpcClient 客户端
 */
public interface IRpcClient {
    <T>T execute(RequestInfo requestInfo, Type cls);
}
