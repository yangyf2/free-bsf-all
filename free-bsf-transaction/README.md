## transaction集成说明
### 介绍
用于支持分布式事务，支持Seata,努力送达型事务(自研),华为servicecomb-pack等。 
* seata: 分布式事务解决方案 
https://seata.io/zh-cn/


### 依赖引用
一般包已经通过free-bsf-starter依赖在脚手架项目中,无需额外配置
``` 
<dependency>
	<artifactId>free-bsf-transaction</artifactId>
	<groupId>com.free.bsf</groupId>
	<version>1.7.1-SNAPSHOT</version>
</dependency>
```

### 配置说明
```
## bsf transaction  集成
##transaction服务的启用开关，[必须]，默认false
bsf.transaction.enabled=false

#seata.enable,默认关闭
bsf.transaction.seata.enable=false

#seata 支持at和xa模式
bsf.transaction.seata.dataSourceProxyMode=at

#seata 事务默认分组
bsf.transaction.seata.txServiceGroup=${spring.application.name}-seata-service-group

#仅seata 注册中心:file模式下,仅临时使用
service.default.grouplist=localhost:8091
```

### 使用说明
```
##AT 模式需要在相关的服务库中添加表
-- for AT mode you must to init this sql for you business database. the seata server not need it.
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `id`            BIGINT(20)   NOT NULL AUTO_INCREMENT COMMENT 'increment id',
    `branch_id`     BIGINT(20)   NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(100) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME     NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME     NOT NULL COMMENT 'modify datetime',
    PRIMARY KEY (`id`),
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8 COMMENT ='AT transaction mode undo table';

```
### 选型 
seata服务端部署:存储采用db,注册中心采用eureka,配置中心采用apollo
### 高级篇 
#### 原理说明 
。
#### 其他
* seata 暂不支持histryx中线程池方式熔断,会影响分布式事务的传递。
* 本集成未严格验证测试.
* seata据说在高并发情况下，会出现事务回滚问题，应该在未来1.5版本修复。
  https://github.com/seata/seata/issues/3419

