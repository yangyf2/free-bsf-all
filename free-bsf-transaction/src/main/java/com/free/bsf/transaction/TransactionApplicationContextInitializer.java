package com.free.bsf.transaction;

import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.PropertyUtils;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StringUtils;

import java.io.File;

/**
 * @author: chejiangyi
 * @version: 2019-06-14 09:41
 **/
public class TransactionApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        this.initializeSystemProperty(context);
    }

    void initializeSystemProperty(ConfigurableApplicationContext context) {
        org.springframework.core.env.Environment environment = context.getEnvironment();
        if("false".equalsIgnoreCase(environment.getProperty(CoreProperties.BsfEnabled))){
            return;
        }
        String propertyValue = environment.getProperty(TransactionProperties.SpringApplicationName);
        if (StringUtils.isEmpty(propertyValue)) {
           return;
        }
        //默认TC集群名
        String seataTxServiceGroup= TransactionProperties.getSeataTxServiceGroup();
        setProperty(TransactionProperties.SeataVgroupMapping.replace("{txServiceGroup}",seataTxServiceGroup),
                "default");
    }

    void setProperty(String key,String propertyValue)
    {
        PropertyUtils.setDefaultInitProperty(TransactionApplicationContextInitializer.class,
                TransactionProperties.Project,key,propertyValue,
                "初始化sentinel配置");
    }
}
