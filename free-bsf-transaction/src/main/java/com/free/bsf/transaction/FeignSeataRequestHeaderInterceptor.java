package com.free.bsf.transaction;

import com.free.bsf.core.apiregistry.CoreRequestInterceptor;
import com.free.bsf.core.apiregistry.RequestInfo;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import io.seata.common.util.StringUtils;
import io.seata.core.context.RootContext;
import lombok.val;

import java.util.HashMap;
import java.util.Map;

public class FeignSeataRequestHeaderInterceptor implements RequestInterceptor, CoreRequestInterceptor {
    @Override
    public void apply(RequestTemplate requestTemplate) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                requestTemplate.header(kv.getKey(),kv.getValue());
            }
        }
    }

    private Map<String,String> newHeader(){
        Map<String,String> header = new HashMap<>();
        String xid = RootContext.getXID();
        if (StringUtils.isNotBlank(xid)) {
            header.put("Seataid", xid);
        }
        return header;
    }

    @Override
    public void append(RequestInfo request) {
        val header = newHeader();
        if(header!=null){
            for(val kv:header.entrySet()){
                request.getHeader().put(kv.getKey(),kv.getValue());
            }
        }
    }
}
