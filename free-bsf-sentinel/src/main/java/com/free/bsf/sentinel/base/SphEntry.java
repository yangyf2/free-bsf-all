package com.free.bsf.sentinel.base;

import com.alibaba.csp.sentinel.EntryType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.util.ObjectUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class SphEntry {
    String name;
    EntryType trafficType=EntryType.IN;
    int batchCount=1;
    Object[] args;

    public boolean hasArgs(){
        return !ObjectUtils.isEmpty(args);
    }
}
