package com.free.bsf.sentinel;

import com.alibaba.csp.sentinel.config.SentinelConfig;
import com.free.bsf.core.config.CoreProperties;
import com.free.bsf.core.util.NetworkUtils;
import com.free.bsf.core.util.PropertyUtils;
import lombok.val;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: chejiangyi
 * @version: 2019-06-14 09:41
 **/
public class SentinelApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        this.initializeSystemProperty(context);
    }

    void initializeSystemProperty(ConfigurableApplicationContext context) {
        org.springframework.core.env.Environment environment = context.getEnvironment();
        if("false".equalsIgnoreCase(environment.getProperty(CoreProperties.BsfEnabled))){
            return;
        }
        String propertyValue = environment.getProperty(SentinelProperties.SpringApplicationName);
        if (StringUtils.isEmpty(propertyValue)) {
           return;
        }
        String propertyValue2 = environment.getProperty(SentinelProperties.BsfSentinelEnabled);
        if (!"true".equalsIgnoreCase(propertyValue2)) {
            return;
        }
        Map<String,String> config = new HashMap<>();
        config.put(SentinelProperties.ProjectName, PropertyUtils.getPropertyCache(SentinelProperties.SpringApplicationName,""));
        config.put(SentinelProperties.CspSentinelLogDir, "logs"+ File.separator+"csp"+File.separator);
        config.put(SentinelProperties.CspSentinelLogUsePid, "true");
        config.put(SentinelProperties.CspSentinelDashboardServer, PropertyUtils.getPropertyCache(SentinelProperties.CspSentinelDashboardServer,""));
        //setProperty(SentinelProperties.CspSentinelHeartBeatClientIp, NetworkUtils.getIpAddress());
        for(val kv:config.entrySet()){
            setProperty(kv.getKey(),kv.getValue());
        }
        for(val kv:config.entrySet()){
            //同步刷新sentinel缓存
            SentinelConfig.setConfig(kv.getKey(),kv.getValue());
        }
    }

    void setProperty(String key,String propertyValue)
    {
        PropertyUtils.setDefaultInitProperty(SentinelApplicationContextInitializer.class,SentinelProperties.Project,key,propertyValue,
                "初始化sentinel配置");
    }
}
