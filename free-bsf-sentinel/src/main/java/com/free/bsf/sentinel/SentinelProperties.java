package com.free.bsf.sentinel;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

public class SentinelProperties {
    public static String Project="Sentinel";
    public static String BsfSentinel="bsf.sentinel.";
    public static String SpringApplicationName="spring.application.name";
    /**
     * sentinel 基础配置
     */
    public static String BsfSentinelEnabled="bsf.sentinel.enabled";
    public static String ProjectName="project.name";
    public static String CspSentinelLogDir="csp.sentinel.log.dir";
    public static String CspSentinelLogUsePid="csp.sentinel.log.use.pid";
    public static String CspSentinelDashboardServer="csp.sentinel.dashboard.server";
    public static String CspSentinelHeartBeatClientIp="csp.sentinel.heartbeat.client.ip";
    /**
     * web 限流包含resource
     */
    public static String BsfSentinelWebIncludeResource="bsf.sentinel.web.includeResource";
    /**
     * feign 限流包含resource
     */
    public static String BsfSentinelFeignIncludeResource="bsf.sentinel.feign.includeResource";
    /**
     * 规则
     */
    public static String BsfSentinelFlowRule="bsf.sentinel.flowRule";
    public static String BsfSentinelDegradeRule="bsf.sentinel.degradeRule";
    public static String BsfSentinelSystemRule="bsf.sentinel.systemRule";
    public static String BsfSentinelAuthorityRule="bsf.sentinel.authorityRule";
    public static String BsfSentinelParamRule="bsf.sentinel.paramRule";
}
