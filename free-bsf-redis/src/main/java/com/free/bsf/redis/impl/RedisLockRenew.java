package com.free.bsf.redis.impl;

import com.free.bsf.core.util.ThreadUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Huang Zhaoping
 * edit by chejiangyi
 * 分布式锁续约
 * @date 2020/5/14
 */
@Slf4j
public class RedisLockRenew {

    private static final Map<String, Task> taskCache = new ConcurrentHashMap<>();
    private static final TaskMonitor taskMonitor = new TaskMonitor();

    public static void add(RedisLock lock) {
        taskCache.put(lock.getClientId(), new Task(lock));
        if (!taskMonitor.isRunning()) {
            taskMonitor.start();
        }
    }

    public static void remove(RedisLock lock) {
        taskCache.remove(lock.getClientId());
    }

    public static void destroy() {
        taskMonitor.stop(true);
    }

    private static long executeTasks() {
        if (taskCache.isEmpty()) {
            return 0;
        }
        // 找到需要刷新的任务
        long current = System.currentTimeMillis(), min = 0;
        List<Task> list = new ArrayList<>();
        for (Task task : taskCache.values()) {
            long time = task.getNextTime();
            if (time <= current) {
                list.add(task);
            } else if (min <= 0 || time < min) {
                min = time;
            }
        }
        // 执行任务列表
        for (Task task : list) {
            try {
                if (!task.execute()) {
                    // 只有锁已经被释放的情况才会失败，直接移除
                    taskCache.remove(task.getClientId());
                }
            } catch (Exception e) {
                log.error("刷新Redis锁异常，3秒后重试", e);
                return current + 3000;
            }
        }
        return min;
    }

    private static class TaskMonitor implements Runnable {
        private volatile boolean isRunning;

        @Override
        public void run() {
            while (isRunning&&!ThreadUtils.system().isShutdown()) {
                long next = executeTasks();
                if (next > 0) {
                    long time = Math.min(Math.max(next - System.currentTimeMillis(), 1000), 60000);
                    ThreadUtils.sleep(time);
                } else if (isIdle()) {
                    stop(false);
                }
            }
        }

        private boolean isIdle() {
            for (int i = 0; i < 60; i++) {
                ThreadUtils.sleep(1000);
                if (taskCache.size() > 0) {
                    return false;
                }
            }
            return true;
        }

        public synchronized void start() {
            if (!isRunning) {
                isRunning = true;
                ThreadUtils.system().submit("RedisLockRenew",()->{
                    run();
                });
            }
        }

        public boolean isRunning() {
            return isRunning;
        }

        public synchronized void stop(boolean force) {
            if (!isRunning) return;
            isRunning = false;
            if (!force && taskCache.size() > 0) {
                isRunning = true;
                return;
            } else if (force) {
                taskCache.clear();
            }
        }
    }

    private static class Task {
        private final RedisLock lock;
        private long executeTime;

        public Task(RedisLock lock) {
            this.lock = lock;
            this.executeTime = System.currentTimeMillis();
        }

        public String getClientId() {
            return lock.getClientId();
        }

        public long getNextTime() {
            return executeTime + lock.getRenewTimeout()*1000;
        }

        public boolean execute() {
            boolean result = lock.doRenew();
            if (result) {
                executeTime = System.currentTimeMillis();
            }
            return result;
        }
    }

}
